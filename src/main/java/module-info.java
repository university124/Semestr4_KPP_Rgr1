module semy.rgr_1 {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires org.kordamp.bootstrapfx.core;
    requires exp4j;

    opens semy.rgr_1 to javafx.fxml;
    exports semy.rgr_1;
}