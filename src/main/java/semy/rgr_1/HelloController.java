package semy.rgr_1;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HelloController {

    @FXML
    private TextField functionField;

    @FXML
    private TextField parameterField;

    @FXML
    private LineChart<Number, Number> lineChart;

    @FXML
    private TableView<DataPoint> dataTable;

    @FXML
    private TableColumn<DataPoint, Number> xColumn;

    @FXML
    private TableColumn<DataPoint, Number> yColumn;

    @FXML
    private TableColumn<DataPoint, Number> dyColumn;

    @FXML
    private Label zoomLabel;

    private Stage stage;

    private ObservableList<DataPoint> dataPoints = FXCollections.observableArrayList();

    private double currentZoom = 1.0;
    private double startX;
    private double startY;

    @FXML
    public void initialize() {
        xColumn.setCellValueFactory(cellData -> cellData.getValue().xProperty());
        yColumn.setCellValueFactory(cellData -> cellData.getValue().yProperty());
        dyColumn.setCellValueFactory(cellData -> cellData.getValue().dyProperty());

        dataTable.setItems(dataPoints);

        // Добавляем обработчики событий для масштабирования и перемещения
        lineChart.setOnScroll(this::handleScroll);
        lineChart.setOnMousePressed(this::handleMousePressed);
        lineChart.setOnMouseDragged(this::handleMouseDragged);
    }

    @FXML
    protected void handleCalculate(ActionEvent event) {
        String functionText = functionField.getText();
        String parameterText = parameterField.getText();

        if (functionText.isEmpty() || parameterText.isEmpty()) {
            showAlert("Input Error", "Please enter both function and parameter.");
            return;
        }

        double a;
        try {
            a = Double.parseDouble(parameterText);
        } catch (NumberFormatException e) {
            showAlert("Input Error", "Parameter 'a' must be a valid number.");
            return;
        }

        try {
            Expression expression = new ExpressionBuilder(functionText)
                    .variables("x", "a")
                    .build()
                    .setVariable("a", a);

            XYChart.Series<Number, Number> functionSeries = new XYChart.Series<>();
            XYChart.Series<Number, Number> derivativeSeries = new XYChart.Series<>();
            dataPoints.clear();

            for (double x = 1.5; x <= 6.5; x += 0.05) {
                expression.setVariable("x", x);
                double y = expression.evaluate();

                // Численное вычисление производной
                double h = 1e-5;
                expression.setVariable("x", x + h);
                double yH = expression.evaluate();
                double dy = (yH - y) / h;

                functionSeries.getData().add(new XYChart.Data<>(x, y));
                derivativeSeries.getData().add(new XYChart.Data<>(x, dy));

                dataPoints.add(new DataPoint(x, y, dy));
            }

            functionSeries.setName("Function");
            derivativeSeries.setName("Derivative");

            lineChart.getData().clear();
            lineChart.getData().addAll(functionSeries, derivativeSeries);
        } catch (Exception e) {
            showAlert("Evaluation Error", "Error in function evaluation: " + e.getMessage());
        }
    }

    @FXML
    protected void handleLoadCSV(ActionEvent event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open CSV File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        File file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            List<Double> xValues = new ArrayList<>();
            List<Double> yValues = new ArrayList<>();
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                String line;
                while ((line = br.readLine()) != null) {
                    String[] values = line.split(",");
                    xValues.add(Double.parseDouble(values[0]));
                    yValues.add(Double.parseDouble(values[1]));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            XYChart.Series<Number, Number> tableSeries = new XYChart.Series<>();
            dataPoints.clear();
            for (int i = 0; i < xValues.size(); i++) {
                double x = xValues.get(i);
                double y = yValues.get(i);
                tableSeries.getData().add(new XYChart.Data<>(x, y));
                dataPoints.add(new DataPoint(x, y, 0)); // Для данных из CSV производная не рассчитывается
            }

            tableSeries.setName("Table Data");
            lineChart.getData().clear();
            lineChart.getData().add(tableSeries);
        }
    }

    @FXML
    private void handleScroll(ScrollEvent event) {
        System.out.println("Scroll event detected: " + event.getDeltaY());
        if (event.getDeltaY() == 0) {
            return;
        }

        double zoomFactor = (event.getDeltaY() > 0) ? 0.9 : 1.1;
        updateZoom(zoomFactor);
    }

    @FXML
    private void handleZoomIn(ActionEvent event) {
        updateZoom(0.9);
    }

    @FXML
    private void handleZoomOut(ActionEvent event) {
        updateZoom(1.1);
    }

    private void updateZoom(double zoomFactor) {
        NumberAxis xAxis = (NumberAxis) lineChart.getXAxis();
        NumberAxis yAxis = (NumberAxis) lineChart.getYAxis();

        double xLowerBound = xAxis.getLowerBound();
        double xUpperBound = xAxis.getUpperBound();
        double yLowerBound = yAxis.getLowerBound();
        double yUpperBound = yAxis.getUpperBound();

        double xRange = xUpperBound - xLowerBound;
        double yRange = yUpperBound - yLowerBound;

        double newXLowerBound = xLowerBound + xRange * (1 - zoomFactor) / 2;
        double newXUpperBound = xUpperBound - xRange * (1 - zoomFactor) / 2;
        double newYLowerBound = yLowerBound + yRange * (1 - zoomFactor) / 2;
        double newYUpperBound = yUpperBound - yRange * (1 - zoomFactor) / 2;

        xAxis.setAutoRanging(false);
        yAxis.setAutoRanging(false);

        xAxis.setLowerBound(newXLowerBound);
        xAxis.setUpperBound(newXUpperBound);
        yAxis.setLowerBound(newYLowerBound);
        yAxis.setUpperBound(newYUpperBound);

        currentZoom *= zoomFactor;
        zoomLabel.setText(String.format("%.0f%%", 1 / currentZoom * 100));

        System.out.println("Zoom applied: x [" + newXLowerBound + ", " + newXUpperBound + "], y [" + newYLowerBound + ", " + newYUpperBound + "]");
    }

    @FXML
    private void handleMousePressed(MouseEvent event) {
        startX = event.getX();
        startY = event.getY();
    }

    @FXML
    private void handleMouseDragged(MouseEvent event) {
        double deltaX = event.getX() - startX;
        double deltaY = event.getY() - startY;

        NumberAxis xAxis = (NumberAxis) lineChart.getXAxis();
        NumberAxis yAxis = (NumberAxis) lineChart.getYAxis();

        double xScale = (xAxis.getUpperBound() - xAxis.getLowerBound()) / xAxis.getWidth();
        double yScale = (yAxis.getUpperBound() - yAxis.getLowerBound()) / yAxis.getHeight();

        double xOffset = deltaX * xScale;
        double yOffset = deltaY * yScale;

        xAxis.setLowerBound(xAxis.getLowerBound() - xOffset);
        xAxis.setUpperBound(xAxis.getUpperBound() - xOffset);
        yAxis.setLowerBound(yAxis.getLowerBound() + yOffset);
        yAxis.setUpperBound(yAxis.getUpperBound() + yOffset);

        startX = event.getX();
        startY = event.getY();
    }

    private void showAlert(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public static class DataPoint {
        private final SimpleDoubleProperty x;
        private final SimpleDoubleProperty y;
        private final SimpleDoubleProperty dy;

        public DataPoint(double x, double y, double dy) {
            this.x = new SimpleDoubleProperty(x);
            this.y = new SimpleDoubleProperty(y);
            this.dy = new SimpleDoubleProperty(dy);
        }

        public double getX() {
            return x.get();
        }

        public SimpleDoubleProperty xProperty() {
            return x;
        }

        public double getY() {
            return y.get();
        }

        public SimpleDoubleProperty yProperty() {
            return y;
        }

        public double getDy() {
            return dy.get();
        }

        public SimpleDoubleProperty dyProperty() {
            return dy;
        }
    }
}
